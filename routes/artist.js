var express = require('express');
var router = express.Router();
let Artist = require("../modules/Artist")
let artist = new Artist({})
router.post('/add', function(req, res, next) {
    artist.add(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.put('/edit', function(req, res, next) {
    artist.edit(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.delete('/delete', function(req, res, next) {
    artist.delete(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.get('/get', function(req, res, next) {
    artist.get_artist(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.query)
});

module.exports = router;