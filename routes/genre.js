var express = require('express');
var router = express.Router();
let Genre = require("../modules/Genre")
let genre = new Genre({})
router.post('/add', function(req, res, next) {
    genre.add(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.put('/edit', function(req, res, next) {
    genre.edit(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.delete('/delete', function(req, res, next) {
    genre.delete(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.get('/get', function(req, res, next) {
    genre.get_genre(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.query)
});

module.exports = router;