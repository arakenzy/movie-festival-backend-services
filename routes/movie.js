var express = require('express');
var router = express.Router();
let Movie = require("../modules/Movie")
let movie = new Movie({})
router.post('/add', function(req, res, next) {
    movie.add(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.put('/edit', function(req, res, next) {
    movie.edit(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.delete('/delete', function(req, res, next) {
    movie.delete(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.body)
});

router.get('/get', function(req, res, next) {
    movie.get_movie(function (param) {
        res.status(param.status).json(param.chunk)
    }, req.query)
});

module.exports = router;