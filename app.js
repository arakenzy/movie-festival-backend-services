var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mysql = require('mysql2');
const bluebird = require('bluebird');

global.user = 1
global.viewer_user = {
    "token1assalam2020" : {"id_userdetail" : 1, "name" : "chandra", "email" : "chandra.yuda.andika@gmail.com"}
}



global.log = require('./config/logger.js');
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password : 'root',
    port: '8889',
    database: 'festival',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});

global.promisePool = pool.promise();

var indexRouter = require('./routes/index');
var genreRouter = require('./routes/genre');
var artistRouter = require('./routes/artist');
var movieRouter = require('./routes/movie');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/genre', genreRouter);
app.use('/artist', artistRouter);
app.use('/movie', movieRouter);

module.exports = app;
