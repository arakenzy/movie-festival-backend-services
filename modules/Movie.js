/******************************************
 * dictionary
 *  -mha movie_having_artist
 *  -mhg movie_having_genre
 *  -mhwu movie_having_watch_url
 *  -uhm userdetail_having_movie
 * @constructor
 */
function Movie() {

}

Movie.prototype.add = async function(fn,params){
    let result = {}
    console.log(params)
    try{
        const [rows, fields] = await promisePool.execute('insert into movie (title,description,duration,created_by) values(?,?,?,?)', [params.title, params.description,params.duration, global.user]);
        this.add_movie_artist(rows.insertId,params.artist)
        this.add_movie_genre(rows.insertId,params.genre)
        this.add_movie_watch_url(rows.insertId,params.watch_url)
        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Movie.",serverMessage :"Berhasil Menambahkan Data Movie."  ,code : 10002}}
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Movie.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        fn(result)
    }

}

Movie.prototype.add_movie_artist = async function(id_movie, artist){
    let result = {}
    try{
        let arrMovieartist = []
        for(i in artist){
            let arrArtist = []
                arrArtist.push(id_movie)
                arrArtist.push(artist[i])
            arrMovieartist.push(arrArtist)
        }
        const [data_mhaDeleted] = await promisePool.execute('delete from movie_having_artist where id_movie = (?) ', [id_movie]);
        const [data_mha] = await promisePool.execute('insert into movie_having_artist (id_movie,id_artist) values ?', [arrMovieartist]);

        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Movie.",serverMessage :"Berhasil Menambahkan Data Movie."  ,code : 10002}}
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Movie.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        log.error(result)
    }
}

Movie.prototype.add_movie_watch_url = async function(id_movie, watch_url){
    let result = {}
    try{
        let arrMovieUrl = []
        for(i in watch_url){
            let arrUrl = []
            arrUrl.push(id_movie)
            arrUrl.push(artist[i])
            arrMovieUrl.push(arrUrl)
        }
        const [data_mhaDeleted] = await promisePool.execute('delete from movie_having_artist where id_movie = (?)', [id_movie]);
        const [data_mha] = await promisePool.execute('insert into movie_having_artist (id_movie,id_artist) values ?', arrMovieUrl);

        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Movie.",serverMessage :"Berhasil Menambahkan Data Movie."  ,code : 10002}}
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Movie.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        log.error(result)
    }
}

Movie.prototype.add_movie_genre = async function(id_movie, genre){
    let result = {}
    try{
        let arrMovieGenre = []
        for(i in genre){
            let arrGenre = []
            arrGenre.push(id_movie)
            arrGenre.push(genre[i])
            arrMovieGenre.push(arrGenre)
        }
        const [data_mhaDeleted] = await promisePool.execute('delete from movie_having_genre where id_movie = (?)', [id_movie]);
        const [data_mha] = await promisePool.execute('insert into movie_having_genre (id_movie,id_artist) values ?', arrMovieGenre);

        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Movie.",serverMessage :"Berhasil Menambahkan Data Movie."  ,code : 10002}}
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Movie.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        log.error(result)
    }
}


Movie.prototype.edit = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('update movie set title = ?, description = ?,duration = ?, updated_date = now(), updated_by = ? where id_movie = ?', [params.title,params.description,params.duration, global.user,params.id_movie]);
        this.add_movie_artist(rows.insertId,params.artist)
        this.add_movie_genre(rows.insertId,params.genre)
        this.add_movie_watch_url(rows.insertId,params.watch_url)
        if(rows.changedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Edit Data Movie.",serverMessage : "Berhasil Edit Data Movie." ,code : 10003}}
        }else{
            result = {status : 400, chunk : {data : rows, clientMessage : "Data Movie Tidak Ditemukan.",serverMessage : "Data Movie Tidak Ditemukan." ,code : 10009}}
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Edit Data Movie.",serverMessage : e.message,serverMessageCode : e.code ,code : 10004}}
        fn(result)
    }

}

Movie.prototype.delete = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('delete from movie where id_movie = ?', [params.id_movie]);
        if(rows.affectedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Delete Data Movie.",serverMessage : "Berhasil Delete Data Movie." ,code : 10005}}
        }else{
            result = {status: 400,
                chunk: {
                    data: rows,
                    clientMessage: "Data Movie Tidak Ditemukan.",
                    serverMessage: "Data Movie Tidak Ditemukan.",
                    code: 10010
                }
            }
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Delete Data Movie.",serverMessage : e.message ,code : 10006}}
        fn(result)
    }
}

Movie.prototype.get_movie = async function(fn,params){
    let result = {}
    try{
        let paging = params.page || 1
        let limit = params.limit || 10
        let offset = (paging-1) * limit
        const [rows, fields] = await promisePool.execute('select * from movie order by id_movie limit ? offset ?', [limit, offset]);
        result = {status: 200,
            chunk: {
                data: rows,
                clientMessage: "Berhasil Mendapatkan Data Movie.",
                serverMessage: "Berhasil Mendapatkan Data Movie.",
                code: 10007
            }
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Mendapatkan Data Movie.",serverMessage : e.message ,code : 10008}}
        fn(result)
    }

}


module.exports = Movie