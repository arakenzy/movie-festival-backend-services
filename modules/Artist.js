function Artist() {

}

Artist.prototype.add = async function(fn,params){
    let result = {}
    try{
        console.log(params)
        const [rows, fields] = await promisePool.execute('insert into artist (name,description,created_by) values(?,?,?)', [params.name, params.description, global.user]);
        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Artist.",serverMessage :"Berhasil Menambahkan Data Artist."  ,code : 10002}}
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Artist.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        fn(result)
    }

}

Artist.prototype.edit = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('update artist set name = ?, description = ?, updated_date = now(), updated_by = ? where id_artist = ?', [params.name,params.description, global.user,params.id_artist]);
        if(rows.changedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Edit Data Artist.",serverMessage : "Berhasil Edit Data Artist." ,code : 10003}}
        }else{
            result = {status : 400, chunk : {data : rows, clientMessage : "Data Artist Tidak Ditemukan.",serverMessage : "Data Artist Tidak Ditemukan." ,code : 10009}}
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Edit Data Artist.",serverMessage : e.message,serverMessageCode : e.code ,code : 10004}}
        fn(result)
    }

}

Artist.prototype.delete = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('delete from artist where id_artist = ?', [params.id_artist]);
        if(rows.affectedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Delete Data Artist.",serverMessage : "Berhasil Delete Data Artist." ,code : 10005}}
        }else{
            result = {status: 400,
                chunk: {
                    data: rows,
                    clientMessage: "Data Artist Tidak Ditemukan.",
                    serverMessage: "Data Artist Tidak Ditemukan.",
                    code: 10010
                }
            }
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Delete Data Artist.",serverMessage : e.message ,code : 10006}}
        fn(result)
    }
}

Artist.prototype.get_artist = async function(fn,params){
    let result = {}
    try{
        let paging = params.page || 1
        let limit = params.limit || 10
        let offset = (paging-1) * limit
        const [rows, fields] = await promisePool.execute('select * from artist order by id_artist limit ? offset ?', [limit, offset]);
        result = {status: 200,
            chunk: {
                data: rows,
                clientMessage: "Berhasil Mendapatkan Data Artist.",
                serverMessage: "Berhasil Mendapatkan Data Artist.",
                code: 10007
            }
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Mendapatkan Data Artist.",serverMessage : e.message ,code : 10008}}
        fn(result)
    }

}

module.exports = Artist