function Genre() {

}

Genre.prototype.add = async function(fn,params){
    let result = {}
    try{
        console.log(params)
        const [rows, fields] = await promisePool.execute('insert into genre (name,description,created_by) values(?,?,?)', [params.name, params.description, global.user]);
        result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Menambahkan Data Genre.",serverMessage :"Berhasil Menambahkan Data Genre."  ,code : 10002}}
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Menambah Data Genre.",serverMessage : e.message,serverMessageCode : e.code,serverMessage : e.message ,code : 10001}}
        fn(result)
    }
    
}

Genre.prototype.edit = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('update genre set name = ?, description = ?, updated_date = now(), updated_by = ? where id_genre = ?', [params.name,params.description, global.user,params.id_genre]);
        if(rows.changedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Edit Data Genre.",serverMessage : "Berhasil Edit Data Genre." ,code : 10003}}
        }else{
            result = {status : 400, chunk : {data : rows, clientMessage : "Data Genre Tidak Ditemukan.",serverMessage : "Data Genre Tidak Ditemukan." ,code : 10009}}
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Edit Data Genre.",serverMessage : e.message,serverMessageCode : e.code ,code : 10004}}
        fn(result)
    }

}

Genre.prototype.delete = async function(fn,params){
    let result = {}
    try{
        const [rows, fields] = await promisePool.execute('delete from genre where id_genre = ?', [params.id_genre]);
        if(rows.affectedRows > 0){
            result = {status : 200, chunk : {data : rows, clientMessage : "Berhasil Delete Data Genre.",serverMessage : "Berhasil Delete Data Genre." ,code : 10005}}
        }else{
            result = {status: 400,
                chunk: {
                    data: rows,
                    clientMessage: "Data Genre Tidak Ditemukan.",
                    serverMessage: "Data Genre Tidak Ditemukan.",
                    code: 10010
                }
            }
        }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Delete Data Genre.",serverMessage : e.message ,code : 10006}}
        fn(result)
    }
}

Genre.prototype.get_genre = async function(fn,params){
    let result = {}
    try{
        let paging = params.page || 1
        let limit = params.limit || 10
        let offset = (paging-1) * limit
        const [rows, fields] = await promisePool.execute('select * from genre order by id_genre limit ? offset ?', [limit, offset]);
            result = {status: 200,
                chunk: {
                    data: rows,
                    clientMessage: "Berhasil Mendapatkan Data Genre.",
                    serverMessage: "Berhasil Mendapatkan Data Genre.",
                    code: 10007
                }
            }
        fn(result)
    }catch (e) {
        result = {status : 400,chunk :{clientMessage : "Gagal Mendapatkan Data Genre.",serverMessage : e.message ,code : 10008}}
        fn(result)
    }

}

module.exports = Genre